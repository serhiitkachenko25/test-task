import NavBar from "./components/NavBar";
import Hero from "./components/Hero";
import Portfolio from "./components/Portfolio";
import Partners from "./components/Partners";
import About from "./components/About";
import Blog from "./components/Blog";
import Contact from "./components/Contact";
import Footer from "./components/Footer";

function App() {
  return (
    <div className='App'>
      <NavBar />
      <Hero />
      <Portfolio />
      <Partners />
      <About />
      <Blog />
      <Contact />
      <Footer />
    </div>
  );
}

export default App;
