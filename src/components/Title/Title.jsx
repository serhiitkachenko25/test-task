import { Row, Col } from "react-simple-flex-grid";

import "./Title.scss";

export default function Title(props) {
  return (
    <Row>
      <Col className='big__title'>
        <h2 className='weight800 font60 padding40'>{props.title}</h2>
      </Col>
    </Row>
  );
}
