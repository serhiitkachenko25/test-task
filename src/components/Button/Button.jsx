import React from "react";
import { Link } from "react-scroll";

import "./Button.scss";

export default function Button(props) {
  return (
    <Link
      className='button'
      to={props.target}
      spy={true}
      smooth={true}
      offset={-70}
      duration={500}
    >
      {props.label}
    </Link>
  );
}
