import { Link } from "react-scroll";
import "./Footer.scss";

import Logo from "../../icons/logo/logo-white.svg";
import Arrow from "../../icons/arrow/arrow-white.svg";

export default function Footer() {
  return (
    <div className='footer'>
      <div className='wrapper'>
        <ul className='list'>
          <li>
            <div className='footer-box'>
              <img src={Logo} alt='logo' />
              <p>© 2020 - Mint,All Right Reserved</p>
            </div>
          </li>
          <li>
            <Link to='hero' spy={true} smooth={true} offset={0} duration={500}>
              <div className='footer-box back-to-top'>
                <p>BACK TO TOP</p>
                <img src={Arrow} alt='arrow' />
              </div>
            </Link>
          </li>
        </ul>
      </div>
    </div>
  );
}
