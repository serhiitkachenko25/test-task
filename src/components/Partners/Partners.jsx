import Swiper from "./Swiper";

import "./Partners.scss";

export default function Partners() {
  return (
    <div id='partners'>
      <div className='wrapper'>
        <Swiper />
      </div>
    </div>
  );
}
