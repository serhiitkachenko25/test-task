import "./PartnerBox.scss";

export default function partnerBox(props) {
  return (
    <div className='partner__box flex-center'>
      <img src={props.partner} alt='partner' />
    </div>
  );
}
