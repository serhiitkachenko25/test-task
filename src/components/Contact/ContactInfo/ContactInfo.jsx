import "./ContactInfo.scss";

import ContactInfoBox from "./ContactInfoBox";

import ContactInfoIcon1 from "../../../icons/contact/contact-info-icon1.svg";
import ContactInfoIcon2 from "../../../icons/contact/contact-info-icon2.svg";
import ContactInfoIcon3 from "../../../icons/contact/contact-info-icon3.svg";

export default function contactInfo() {
  return (
    <ul className='contact__list'>
      <li className='contact__info'>
        <ContactInfoBox
          icon={ContactInfoIcon1}
          textLine1='1211 Awesome Avenue,'
          textLine2='NY USD'
        />
      </li>
      <li className='contact__info'>
        <ContactInfoBox
          icon={ContactInfoIcon2}
          textLine1='+00 123 - 456 -78'
          textLine2='+00 987 - 654 -32'
        />
      </li>
      <li className='contact__info'>
        <ContactInfoBox
          icon={ContactInfoIcon3}
          textLine1='mint@mintmail.com'
          textLine2=''
        />
      </li>
    </ul>
  );
}
