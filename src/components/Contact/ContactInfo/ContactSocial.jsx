import "./ContactSocial.scss";

import Facebook from "../../../icons/social/facebook.svg";
import Twitter from "../../../icons/social/twitter.svg";
import Dribble from "../../../icons/social/dribble.svg";

export default function contactSocial() {
  return (
    <ul className='contact-list'>
      <li className='contact__social'>
        <img src={Facebook} alt='facebook' />
      </li>
      <li className='contact__social'>
        <img src={Twitter} alt='Twitter' />
      </li>
      <li className='contact__social'>
        <img src={Dribble} alt='Dribble' />
      </li>
    </ul>
  );
}
