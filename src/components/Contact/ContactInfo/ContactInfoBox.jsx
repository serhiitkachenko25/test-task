import "./ContactInfoBox.scss";

export default function contactInfoBox(props) {
  return (
    <div className='contact__info-box'>
      <div>
        <img src={props.icon} alt='address' />
      </div>
      <div>
        <p>{props.textLine1}</p>
        <p>{props.textLine2}</p>
      </div>
    </div>
  );
}
