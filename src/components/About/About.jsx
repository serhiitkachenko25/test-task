import { Row, Col } from "react-simple-flex-grid";

import TeamBox from "./TeamBox";
import TeamInfo from "./TeamInfo";
import Title from "../Title";

import "./About.scss";
import Person01 from "../../images/about/person01.png";
import Person02 from "../../images/about/person02.png";

export default function About() {
  return (
    <div id='about'>
      <div className='wrapper'>
        <Title title='ABOUT US.' />
        <p className='font12'>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmod tempor incididunt<br></br>ut labore et dolore magna aliqua.
        </p>
        <Row className='team'>
          <Col md={12} lg={4}>
            <TeamBox
              avatar={Person01}
              name='Luke Skywalker'
              job='Web designer'
            />
          </Col>
          <Col md={12} lg={4}>
            <TeamBox avatar={Person02} name='Han Solo' job='Graphic Designer' />
          </Col>
          <Col md={12} lg={4}>
            <TeamInfo />
          </Col>
        </Row>
      </div>
    </div>
  );
}
