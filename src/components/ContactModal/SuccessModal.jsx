import "./Modal.scss";

export default function SuccessModal(props) {
  return (
    <div className='sucess__modal'>
      <p>Success!</p>
      <p>
        Thank you for your email.<br></br>We will respond as soon as possible.
      </p>
      <div
        role='button'
        tabIndex={0}
        className='modal__btn flex-center'
        onClick={props.closeModal}
        onKeyDown={props.closeModal}
      >
        <p>Ok</p>
      </div>
    </div>
  );
}
