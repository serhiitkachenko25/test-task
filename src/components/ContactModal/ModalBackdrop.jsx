import "./Modal.scss";

export default function Backdrop(props) {
  return (
    <div
      role='button'
      aria-label='Close'
      tabIndex={0}
      className='backdrop'
      onClick={props.closeModal}
      onKeyDown={props.closeModal}
    ></div>
  );
}
