import "./Modal.scss";

export default function ErrorModal(props) {
  return (
    <div className='error__modal'>
      <p>Oooops!!!</p>
      <p>Something went wrong :{"("}</p>
      <div
        role='button'
        tabIndex={0}
        className='modal__btn flex-center'
        onClick={props.closeModal}
        onKeyDown={props.closeModal}
      >
        <p>Ok</p>
      </div>
    </div>
  );
}
