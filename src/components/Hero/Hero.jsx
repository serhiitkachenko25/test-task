import { Row, Col } from "react-simple-flex-grid";

import "./hero.scss";

import HeroImage from "../../images/hero/hero-image.png";

import Button from "../Button";

export default function Hero() {
  return (
    <div className='hero' id='hero'>
      <div className='wrapper'>
        <Row className='hero-grid'>
          <Col md={12} lg={6} className='hero-info'>
            <h1 className='weight800 font60'>Hello!!!</h1>
            <h1 className='weight800 font60'>
              We Are Creative Digital Agency.
            </h1>
            <p className='font12'>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
              doeiusmod tempor incididunt ut labore et dolore magna aliqua
            </p>
            <Button label='SEND MESSAGE' target={"contact"} />
          </Col>
          <Col md={12} lg={6}>
            <img src={HeroImage} alt='hero' className='hero-image' />
          </Col>
        </Row>
      </div>
    </div>
  );
}
